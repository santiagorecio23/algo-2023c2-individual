package aed;

class Funciones {
    int cuadrado(int x) {
        // COMPLETAR
            int res = x * x;

        return res;
    }

    double distancia(double x, double y) {
        // COMPLETAR
        int res = sqrt(cuadrado(x)+ cuadrado(x));
        return res;
    }

    boolean esPar(int n) {
        // COMPLETAR
        boolean res;
        if (n % 2 == 0 ){
            res=true;
        }else{
            res=false;
        }
        return res;
    }

    boolean esBisiesto(int n) {
        // COMPLETAR
        boolean res;
        if (n % 400 == 0 ) {
            res = true;
        } else if (((n % 4) == 0) && ((n % 100) != 0)) {
            res = true ;
        } else {
            res = false;
        }
        return res;
    }

    int factorialIterativo(int n) {
        // COMPLETAR
        int res = 0;
        int factorial = 1;
        if ( n == 0){
            res = 1;
        }else{
            
            for ( int i = 2 ; i <=n ; i++){
                factorial *= i;
            }

            res=factorial;

        }
        
        return res;
    }

    int factorialRecursivo(int n) {
        // COMPLETAR
        int res = 0;
        if ( n == 0 || n == 1){
            res = 1;
        }
        if ( n >= 2){
            res = n * factorialRecursivo(n-1);
        }
        return res;
    }

    boolean esPrimo(int n) {
        // COMPLETAR
        int i =2;
        boolean control = true;
        int res;
        while ( (i<=n) && (control==true)){
            if ( (n % i) == 0 ){
                control= false;
                res=false;
                i=i+1;
            }else{
                res=true;
                i=i+1;
            }

        }
        
        return res;
    }

    int sumatoria(int[] numeros) {
        // COMPLETAR
        int i=0;
        int res;
        while (i < numeros.length){
            res = res + numeros[i]
            i=i+1;
        }
        return res;
    }

    int busqueda(int[] numeros, int buscado) {
        // COMPLETAR
        int res;
        int i =0;
        boolean control=true;
        while ((i < numeros.length) && (control==true)){
            if ( buscado == numeros[i]){
                control =false;
                res=i;
                i=i+1;
            }else{
                control=false;
                i=i+1;
            }

        }
        return res;
    }

    boolean tienePrimo(int[] numeros) {
        // COMPLETAR
        boolean res;
        boolean control=true;
        int i =0;
        while((i < numeros.length) && (control==true)){
            if ( esPrimo(numeros[i]) == true){
                control=false;
                res=true;
                i=i+1;
            }else{
                res=false;
                i=i+1;
            }
        }
        return res;
    }

    boolean todosPares(int[] numeros) {
        // COMPLETAR
        boolean res;
        boolean control=true;
        int i =0;
        while( (i < numeros.length) && (control==true)){
            if( (numeros[i] % 2) != 0 ){
                control = false;
                res=false;
                i=i+1;
            }else{
                res=true;
                i=i+1;
            }
        }
        return res;
    }

    boolean esPrefijo(String s1, String s2) {
        // COMPLETAR
        boolean res;

        if (s1.length()> s2.length){
            res=false;
        }else{
            if (s1.length()<= s2.length()){
                boolean control = true;
                int i = 0;
                while( (i < s1.length()) && (control==true)){
                    if( s1.charAt(i)!= s2.charAt(i)){
                        control=false;
                        res=false;
                        i=i+1;
                    }else{
                        res=true;
                        i=i+1;
                    }
                }
            }
        }

        return res;
    }

    boolean esSufijo(String s1, String s2) {
        // COMPLETAR
        
        bool res ;

        if (s1.length()> s2.length){
            res=false;
        }else{
            if (s1.length()<= s2.length()){
                boolean control = true;
                int i = s1.length();
                while( (i < s2.length()) && (control==true)){
                    if( s1.charAt(i- s1.length()) != s2.charAt(i)){
                        control=false;
                        res=false;
                        i=i+1;
                    }else{
                        res=true;
                        i=i+1;
                    }
                }
            }
        }

        return false;
    }
}
