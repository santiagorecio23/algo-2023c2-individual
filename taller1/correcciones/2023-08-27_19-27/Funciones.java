package aed;

class Funciones {
    int cuadrado(int x) {
        // COMPLETAR
        if ( x == 0){
            return 0;
        }else{
        int res = x*x;
        return res;}
    }

    double distancia(double x, double y) {  
        // COMPLETAR
        double res = Math.sqrt((x*x)+ (y*y));
        return res;
    }

    boolean esPar(int n) {
        // COMPLETAR
    
        if ((n % 2) == 0 ){
            return true;
        }else{
            return false;
        }
    }

    boolean esBisiesto(int n) {
        // COMPLETAR

        if (((n % 400) == 0 )|| (((n % 4) == 0) && ((n % 100) != 0))) {
            return true;
        } else {
            return false;
        }
    }

    int factorialIterativo(int n) {
        // COMPLETAR
        int res = 1;
        for (int i = 1; i <= n; i++ ){
            res *= i;
        }
        return res;
    }

    int factorialRecursivo(int n) {
        // COMPLETAR
        
        if ( n == 0 || n == 1){
            return 1;
        }else{
            return n * factorialRecursivo(n-1);

        }    
    }
    

    boolean esPrimo(int n) {
        // COMPLETAR
        if (n <= 1 ){
            return false;
        }
        if ( n <= 3){
            return true;
        }
        if ( n % 2 == 0){
            return false;
        }
        for (int i =2; i < n; i++){
            if ( n % i == 0){
                return false;
            }
        }

        
        return true;
    }

    int sumatoria(int[] numeros) {
        // COMPLETAR
        int res=0;
        if (numeros.length== 0 ){
            return 0;
        }
        for (int i = 0; i < numeros.length ;i++ ){
            res += numeros[i];
        }
        return res;
    }

    int busqueda(int[] numeros, int buscado) {
        // COMPLETAR
        for (int i = 0 ; i < numeros.length; i++){
            if ( numeros[i]== buscado){
                return i ;
            }
        }
        return -1;
    }   

    boolean tienePrimo(int[] numeros) {
        // COMPLETAR
        for ( int numero : numeros){
            if ( esPrimo(numero)){
                return true;
            }
        }
        return false;
    }

    boolean todosPares(int[] numeros) {
        // COMPLETAR
        for (int numero : numeros){
            if (esPar(numero) == false){
                return false;
            }
        }
        return true;
    }

    boolean esPrefijo(String s1, String s2) {
        // COMPLETAR

        if (s1.length()> s2.length()){
            return false;
        }
        for ( int i = 0; i < s1.length(); i++){
            if ( s1.charAt(i) != s2.charAt(i)){
                return false;
            }
        }

        return true;
    }

    boolean esSufijo(String s1, String s2) {
        // COMPLETAR
        if ( s1.length() > s2.length()){
            return false;
        }

        int inicio = s2.length() - s1.length();
        for ( int i =0; i< s1.length();i++){
            if ( s1.charAt(i)!= s2.charAt(inicio + i)){
                return false;
            }
        }
        return true;
    }    
}

