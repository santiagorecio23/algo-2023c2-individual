package aed;
import java.util.*;

public class ListaEnlazada<T> implements Secuencia<T> {
    private class Nodo {
        T elemento;
        Nodo siguiente;
        Nodo anterior; // New field for bidirectional link

        Nodo(T elem) {
            this.elemento = elem;
            this.siguiente = null;
            this.anterior = null;
        }
    }

    private Nodo primero;
    private Nodo ultimo; // New field to keep track of the last node
    private int size;

    public ListaEnlazada() {
        primero = null;
        ultimo = null;
        size = 0;
    }

    public int longitud() {
        return size;
    }

    public void agregarAdelante(T elem) {
        Nodo nuevoNodo = new Nodo(elem);
        if (primero == null) {
            primero = nuevoNodo;
            ultimo = nuevoNodo;
        } else {
            nuevoNodo.siguiente = primero;
            primero.anterior = nuevoNodo;
            primero = nuevoNodo;
        }
        size++;
    }

    public void agregarAtras(T elem) {
        Nodo nuevoNodo = new Nodo(elem);
        if (ultimo == null) {
            primero = nuevoNodo;
            ultimo = nuevoNodo;
        } else {
            nuevoNodo.anterior = ultimo;
            ultimo.siguiente = nuevoNodo;
            ultimo = nuevoNodo;
        }
        size++;
    }

    public T obtener(int i) {
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }

        Nodo actual = (i < size / 2) ? primero : ultimo;
        int index = (i < size / 2) ? i : size - 1 - i;
        while (index > 0) {
            if (i < size / 2) {
                actual = actual.siguiente;
            } else {
                actual = actual.anterior;
            }
            index--;
        }
        return actual.elemento;
    }

    public void eliminar(int i) {
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }

        Nodo nodoAEliminar = (i < size / 2) ? primero : ultimo;
        int index = (i < size / 2) ? i : size - 1 - i;

        while (index > 0) {
            if (i < size / 2) {
                nodoAEliminar = nodoAEliminar.siguiente;
            } else {
                nodoAEliminar = nodoAEliminar.anterior;
            }
            index--;
        }

        if (nodoAEliminar.anterior != null) {
            nodoAEliminar.anterior.siguiente = nodoAEliminar.siguiente;
        } else {
            primero = nodoAEliminar.siguiente;
        }

        if (nodoAEliminar.siguiente != null) {
            nodoAEliminar.siguiente.anterior = nodoAEliminar.anterior;
        } else {
            ultimo = nodoAEliminar.anterior;
        }

        size--;
    }

    public void modificarPosicion(int indice, T elem) {
        if (indice < 0 || indice >= size) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }

        Nodo actual = (indice < size / 2) ? primero : ultimo;
        int index = (indice < size / 2) ? indice : size - 1 - indice;
        while (index > 0) {
            if (indice < size / 2) {
                actual = actual.siguiente;
            } else {
                actual = actual.anterior;
            }
            index--;
        }
        actual.elemento = elem;
    }

    public ListaEnlazada<T> copiar() {
        ListaEnlazada<T> copia = new ListaEnlazada<>();
        Nodo actual = primero;
        while (actual != null) {
            copia.agregarAtras(actual.elemento);
            actual = actual.siguiente;
        }
        return copia;
    }

    public ListaEnlazada(ListaEnlazada<T> lista) {
        this();
        Nodo actual = lista.primero;
        while (actual != null) {
            agregarAtras(actual.elemento);
            actual = actual.siguiente;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Nodo actual = primero;
        while (actual != null) {
            sb.append(actual.elemento);
            if (actual.siguiente != null) {
                sb.append(", ");
            }
            actual = actual.siguiente;
        }
        sb.append("]");
        return sb.toString();
    }

    private class ListaIterador implements Iterador<T> {
        private Nodo actual = primero;
        private Nodo anterior = null;
    
        public boolean haySiguiente() {
            return actual != null;
        }
    
        public boolean hayAnterior() {
            return anterior != null;
        }
    
        public T siguiente() {
            if (!haySiguiente()) {
                throw new NoSuchElementException("No hay elemento siguiente");
            }
            T elemento = actual.elemento;
            anterior = actual;
            actual = actual.siguiente;
            return elemento;
        }
    
        public T anterior() {
            if (!hayAnterior()) {
                throw new NoSuchElementException("No hay elemento anterior");
            }
            T elemento = anterior.elemento;
            actual = anterior;
            anterior = anterior.anterior;
            return elemento;
        }
    }

    public Iterador<T> iterador() {
        return new ListaIterador();
    }
}