package aed;
import java.util.*;

public class ListaEnlazada<T> implements Secuencia<T> {
    private class Nodo {
        T elemento;
        Nodo siguiente;

        Nodo(T elem) {
            this.elemento = elem;
            this.siguiente = null;
        }
    }

    private Nodo primero;
    private int size;

    public ListaEnlazada() {
        primero = null;
        size = 0;
    }

    public int longitud() {
        return size;
    }

    public void agregarAdelante(T elem) {
        Nodo nuevoNodo = new Nodo(elem);
        nuevoNodo.siguiente = primero;
        primero = nuevoNodo;
        size++;
    }

    public void agregarAtras(T elem) {
        Nodo nuevoNodo = new Nodo(elem);
        if (primero == null) {
            primero = nuevoNodo;
        } else {
            Nodo actual = primero;
            while (actual.siguiente != null) {
                actual = actual.siguiente;
            }
            actual.siguiente = nuevoNodo;
        }
        size++;
    }

    public T obtener(int i) {
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }

        Nodo actual = primero;
        for (int j = 0; j < i; j++) {
            actual = actual.siguiente;
        }
        return actual.elemento;
    }

    public void eliminar(int i) {
        if (i < 0 || i >= size) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }

        if (i == 0) {
            primero = primero.siguiente;
        } else {
            Nodo anterior = null;
            Nodo actual = primero;
            for (int j = 0; j < i; j++) {
                anterior = actual;
                actual = actual.siguiente;
            }
            anterior.siguiente = actual.siguiente;
        }
        size--;
    }

    public void modificarPosicion(int indice, T elem) {
        if (indice < 0 || indice >= size) {
            throw new IndexOutOfBoundsException("Index out of bounds");
        }

        Nodo actual = primero;
        for (int i = 0; i < indice; i++) {
            actual = actual.siguiente;
        }
        actual.elemento = elem;
    }

    public ListaEnlazada<T> copiar() {
        ListaEnlazada<T> copia = new ListaEnlazada<>();
        Nodo actual = primero;
        while (actual != null) {
            copia.agregarAtras(actual.elemento);
            actual = actual.siguiente;
        }
        return copia;
    }

    public ListaEnlazada(ListaEnlazada<T> lista) {
        this();
        Nodo actual = lista.primero;
        while (actual != null) {
            agregarAtras(actual.elemento);
            actual = actual.siguiente;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Nodo actual = primero;
        while (actual != null) {
            sb.append(actual.elemento);
            if (actual.siguiente != null) {
                sb.append(", ");
            }
            actual = actual.siguiente;
        }
        sb.append("]");
        return sb.toString();
    }

    private class ListaIterador implements Iterador<T> {
        private Nodo actual = primero;
        private Nodo anterior = null;

        public boolean haySiguiente() {
            return actual != null;
        }

        public boolean hayAnterior() {
            return anterior != null;
        }

        public T siguiente() {
            if (!haySiguiente()) {
                throw new NoSuchElementException("No hay elemento siguiente");
            }
            T elemento = actual.elemento;
            anterior = actual;
            actual = actual.siguiente;
            return elemento;
        }

        public T anterior() {
            if (!hayAnterior()) {
                throw new NoSuchElementException("No hay elemento anterior");
            }
            T elemento = anterior.elemento;
            actual = anterior;
            anterior = null;
            return elemento;
        }
    }

    public Iterador<T> iterador() {
        return new ListaIterador();
    }
}