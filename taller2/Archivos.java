package aed;

import java.util.Scanner;
import java.io.PrintStream;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {
        float res[] = new float[largo];
        for(int i=0; i<largo; i++){
            res[i] = entrada.nextFloat();
        }
        return res;
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {
        float res[][] = new float[filas][columnas];
        for(int i=0; i<filas; i++){
            res[i] = leerVector(entrada, columnas);
        }
        return res;
    }

    void imprimirPiramide(PrintStream salida, int alto) {
        String asteriscos = "*";
        String espacio = "    ";
        for(int i=0; i<alto; i++){
            salida.println(espacio + asteriscos + espacio);
            asteriscos += "**";
            if(espacio.length()>0)
                espacio = espacio.substring(0, espacio.length()-1);
        }
    }
}
