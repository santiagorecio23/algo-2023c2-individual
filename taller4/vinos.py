import numpy as np

#Ejercicio 1-A: Cargar los datos
archivo='./wine.csv'
datosDeVinos= np.genfromtxt(archivo, delimiter=',' , skip_header=1)

#Ejercicio 1-B: Separar datos
variablesIndependientes= np.array(datosDeVinos[:,:-1])
variablesDependientes = np.array(datosDeVinos[:,-1])



# Ejercicio 1-C: NormalizarYCentrarDatos
def normalizarYCentrarDatos(tablaDeDatos):
    promedios= np.mean(tablaDeDatos,axis=1)
    desviacionEstandar = np.std(tablaDeDatos, axis=0, ddof=0)
    datos1= tablaDeDatos - promedios
    datosCentradosYNormalizados = datos1 / desviacionEstandar
    return datosCentradosYNormalizados 


#Ejercicio 1-D: Calcular Matriz de Covarianza 

def calcularMatrizCovarianza(tablaDeDatos):
    matrizCovarianza= np.cov(tablaDeDatos,rowvar=False)
    return matrizCovarianza

#Ejercicio 1-E: Encontrar el autovalor maximo de la matriz covarianza mediante el metodo de la potencia
"""Como la matriz covarianza de nxn es simetrica y con reales en su diagonal, entonces tiene n autovalores 
( no necesariamente distintos),y cada autoespacio asociado tiene la dimension igual a la multiplicidad del autovalor. 
Entonces puedo aplicar el metodo de la potencia en la matriz Covarianza ya que los autovectores forman una base L.I
"""
def autovalorMaximo (matriz, precision):
    longitud= matriz.shape[0]
    autoVector=np.zeros(longitud)
    autoVector[0]=1

    for i in range(precision):
        vectorRandomAnterior=autoVector
        autoVector= np.dot(matriz,autoVector) 
        autoValor =  np.dot(vectorRandomAnterior,autoVector)/np.dot(vectorRandomAnterior,vectorRandomAnterior)
    return [autoValor, autoVector]
array=np.array([[6,5],[4,5]])
print(autovalorMaximo(array,5))
#Ejercicio 1-F
def autovaloresYautovectores(matriz,n):
    listaAutovalores=np.array([])
    listaAutovectores=np.array([])
