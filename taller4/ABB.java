package aed;

import java.util.*;

// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {

    private class Nodo {
        T elemento;
        Nodo izquierdo;
        Nodo derecho;

        Nodo(T elemento) {
            this.elemento = elemento;
            izquierdo = null;
            derecho = null;
        }
    }

    private Nodo raiz;
    private int cardinalidad;

    public ABB() {
        raiz = null;
        cardinalidad = 0;
    }

    public int cardinal() {
        return cardinalidad;
    }

    public T minimo() {
        if (raiz == null) {
            throw new NoSuchElementException("El árbol está vacío");
        }
        return encontrarMinimo(raiz).elemento;
    }

    private Nodo encontrarMinimo(Nodo nodo) {
        while (nodo.izquierdo != null) {
            nodo = nodo.izquierdo;
        }
        return nodo;
    }

    public T maximo() {
        if (raiz == null) {
            throw new NoSuchElementException("El árbol está vacío");
        }
        return encontrarMaximo(raiz).elemento;
    }

    private Nodo encontrarMaximo(Nodo nodo) {
        while (nodo.derecho != null) {
            nodo = nodo.derecho;
        }
        return nodo;
    }

    public void insertar(T elem) {
        raiz = insertarRec(raiz, elem);
    }

    private Nodo insertarRec(Nodo nodo, T elem) {
        if (nodo == null) {
            cardinalidad++;
            return new Nodo(elem);
        }

        if (elem.compareTo(nodo.elemento) < 0) {
            nodo.izquierdo = insertarRec(nodo.izquierdo, elem);
        } else if (elem.compareTo(nodo.elemento) > 0) {
            nodo.derecho = insertarRec(nodo.derecho, elem);
        }

        return nodo;
    }

    public boolean pertenece(T elem) {
        return buscar(raiz, elem);
    }

    private boolean buscar(Nodo nodo, T elem) {
        if (nodo == null) {
            return false;
        }

        if (elem.compareTo(nodo.elemento) < 0) {
            return buscar(nodo.izquierdo, elem);
        } else if (elem.compareTo(nodo.elemento) > 0) {
            return buscar(nodo.derecho, elem);
        }

        return true;
    }

    public void eliminar(T elem) {
        raiz = eliminarRec(raiz, elem);
    }

    private Nodo eliminarRec(Nodo nodo, T elem) {
        if (nodo == null) {
            return nodo;
        }

        if (elem.compareTo(nodo.elemento) < 0) {
            nodo.izquierdo = eliminarRec(nodo.izquierdo, elem);
        } else if (elem.compareTo(nodo.elemento) > 0) {
            nodo.derecho = eliminarRec(nodo.derecho, elem);
        } else {
            // Node with only one child or no child
            if (nodo.izquierdo == null) {
                nodo = nodo.derecho;
                cardinalidad--; // Decrement the cardinality
            } else if (nodo.derecho == null) {
                nodo = nodo.izquierdo;
                cardinalidad--; // Decrement the cardinality
            } else {
                // Node with two children, get the in-order successor (min value in the right
                // subtree)
                nodo.elemento = encontrarMinimo(nodo.derecho).elemento;
                // Delete the in-order successor
                nodo.derecho = eliminarRec(nodo.derecho, nodo.elemento);
            }
        }

        return nodo;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        inorden(raiz, sb);
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1); // Remove the trailing comma
        }
        return "{" + sb.toString() + "}";
    }

    private void inorden(Nodo nodo, StringBuilder sb) {
        if (nodo == null) {
            return;
        }
        inorden(nodo.izquierdo, sb);
        sb.append(nodo.elemento).append(",");
        inorden(nodo.derecho, sb);
    }

    private class ABB_Iterador implements Iterador<T> {
        private Stack<Nodo> pila;

        ABB_Iterador() {
            pila = new Stack<>();
            pushNodos(raiz);
        }

        private void pushNodos(Nodo nodo) {
            while (nodo != null) {
                pila.push(nodo);
                nodo = nodo.izquierdo;
            }
        }

        public boolean haySiguiente() {
            return !pila.isEmpty();
        }

        public T siguiente() {
            if (!haySiguiente()) {
                throw new NoSuchElementException("No hay elementos siguientes");
            }

            Nodo nodo = pila.pop();
            pushNodos(nodo.derecho);

            return nodo.elemento;
        }
    }

    public Iterador<T> iterador() {
        return new ABB_Iterador();
    }
}